package main

import (
	"context"
	"encoding/json"
	"math"
	"net/http"
	"strconv"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

//TopByCountryResponse represents a container for the top-by-country resultset.
type TopByCountryResponse struct {
	Items []TopByCountry `json:"items"`
}

//TopByCountry represents the top-by-country resultset.
type TopByCountry struct {
	Project   string    `json:"project"`
	Access    string    `json:"access"`
	Year      string    `json:"year"`
	Month     string    `json:"month"`
	Countries []Country `json:"countries"`
}

//CountryData represents an individual country's result in the set.
type CountryData struct {
	Country string `json:"country"`
	Views   int    `json:"views"`
	Rank    int    `json:"rank"`
}

//Country represents an individual country's data within the set.
type Country struct {
	Country   string `json:"country"`
	Views     string `json:"views"`
	Rank      int    `json:"rank"`
	ViewsCiel int    `json:"views_ciel"`
}

//TopByCountryHandler is the HTTP handler for top-by-country API requests.
type TopByCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *TopByCountryHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var countriesData = []CountryData{}
	var response = TopByCountryResponse{Items: make([]TopByCountry, 0)}

	project := strings.TrimRight(strings.ToLower(params.ByName("project")), ".org")
	access := strings.ToLower(params.ByName("access"))
	year := params.ByName("year")
	month := params.ByName("month")

	ctx := context.Background()

	var countriesJSON string

	query := `SELECT "countriesJSON" FROM "local_group_default_T_top_bycountry".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND year = ? AND month = ?`
	if err := s.session.Query(query, project, access, year, month).WithContext(ctx).Consistency(gocql.One).Scan(&countriesJSON); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Query failed; %s", err)
		problem.New(problem.Status(http.StatusInternalServerError))
	}

	if err = json.Unmarshal([]byte(countriesJSON), &countriesData); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problem.New(problem.Status(http.StatusInternalServerError), problem.Detail(err.Error())).WriteTo(w)
	}

	var countries = []Country{}

	for _, s := range countriesData {
		countries = append(countries, Country{
			Country:   s.Country,
			Views:     getIntervalForCeilValue(s.Views),
			Rank:      s.Rank,
			ViewsCiel: s.Views,
		})
	}

	response.Items = append(response.Items, TopByCountry{
		Project:   project,
		Access:    access,
		Year:      year,
		Month:     month,
		Countries: countries,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(problem.Status(http.StatusInternalServerError), problem.Detail(err.Error())).WriteTo(w)
		return
	}
	w.Write(data)
}

func getIntervalForCeilValue(value int) string {

	if value < 100 {
		return "100-999"
	}

	//If value is exactly a power of 10, round down
	if math.Mod(math.Log10(float64(value)), 1) == 0 {
		return getIntervalForCeilValue(value - 1)
	}

	var valueString = strconv.FormatInt(int64(value), 10)
	var valueLength = len(valueString)
	var start = math.Pow10(valueLength - 1)
	var end = math.Pow10(valueLength) - 1
	return (strconv.FormatFloat(start, 'f', 0, 64) + "-" + strconv.FormatFloat(end, 'f', 0, 64))
}
